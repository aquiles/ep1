CC := g++
CPPFLAGS := -Wall 

SRCFILES := $(wildcard src/*.cpp)

all: $(SRCFILES:src/%.cpp=obj/%.o)
	$(CC) $(CPPFLAGS) obj/*.o -o bin/saida -lncurses

obj/%.o : src/%.cpp
	$(CC) $(CPPFLAGS) -c $< -o $@ -I./inc -lncurses

.PHONY: clean
clean: 
	rm -rf obj/*
	rm -rf bin/*

run:
	bin/saida


