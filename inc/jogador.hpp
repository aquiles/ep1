#ifndef JOGADOR_HPP
#define JOGADOR_HPP

#include <iostream>
#include <string>
#include "mapa.hpp"
#include "abstrata.hpp"

class jogador : public abstrata{
public:
		jogador(char charj, int posx, int posy);
		~jogador();
		

		void muda_posicao(bool move, char direcao);
		void muda_posicao();

};

#endif
