#ifndef ARMADILHAS_HPP
#define ARMADILHAS_HPP
#include "abstrata.hpp"

class armadilhas: public abstrata {

public:
	armadilhas();
	~armadilhas();

	void muda_posicao();
	int aleatorio_x();
	int aleatorio_y();
	void perde_jogo(int j);
};

#endif
