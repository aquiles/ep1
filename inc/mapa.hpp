#ifndef MAPA_HPP
#define MAPA_HPP

#include <iostream>
#include <string>
#include "abstrata.hpp"
class mapa : public abstrata{

private:
		char alcance[20][50];

	public:
		mapa();
		~mapa();
		
		void setAlcance();
		void addElemento(char charj,int posx,int posy);
		void rmvElemento(int posy,int posx);
		void muda_posicao();
		void abrePorta();
		void addInimigo(int i,int posx, int posy);
		void ganhaJogo();
		void scolisao(int posx,int posy);
		int icolisao(int j, int posx, int posy);
};

#endif
