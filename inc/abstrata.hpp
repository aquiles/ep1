#ifndef ABSTRATA_HPP
#define ABSTRATA_HPP
class abstrata {
private:
	int posx;
	int posy;
	char charj;

public:
	abstrata();
	int getPosx();
	void setPosx(int posx);
	int getPosy();
	void setPosy(int posy);
	char getCharj();
	void setCharj(char charj);
	virtual void muda_posicao()=0;
};

#endif
