#include "armadilhas.hpp"
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <exception>

using namespace std;

armadilhas::armadilhas() {
	setPosx(0);
	setPosy(0);
	setCharj('@');
}
armadilhas::~armadilhas() {
}



int armadilhas::aleatorio_x() {
	short int ax=rand();
	while (ax < 0 || ax > 49) {

		if (ax < 0)
			ax += 50;
		if (ax > 49)
			ax = ax - 50;
	}
	return ax;

}

int armadilhas::aleatorio_y() {
	short int ay=rand();
	while (ay < 0 || ay > 19) {

		if (ay < 0)
			ay += 20;
		if (ay > 19)
			ay = ay - 20;
	}
	return ay;

}

void armadilhas::muda_posicao() {
}

void armadilhas::perde_jogo(int j){
	if(j<=0)
		throw(exception());


}
