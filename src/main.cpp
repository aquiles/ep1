#include <fstream>
#include <exception>
#include <iostream>
#include <string>
#include <stdlib.h>
#include <unistd.h>
#include <ncurses.h>
#include <stdio_ext.h>
#include "abstrata.hpp"
#include "mapa.hpp"
#include "jogador.hpp"
#include "armadilhas.hpp"

using namespace std;

int main() {
	armadilhas * mob = new armadilhas;

	mapa * mapa1 = new mapa();
	mapa1->setAlcance();
	int i=0;
	jogador * jogador1 = new jogador('@', 3, 3);
	int j=3;
	char a = 'a';

	while (TRUE) {
		try{

		initscr();
		clear();
		keypad(stdscr, TRUE);
		noecho();
		mapa1->addElemento(jogador1->getCharj(), jogador1->getPosx(),jogador1->getPosy());
		mapa1->ganhaJogo();
		mapa1->abrePorta();
		mob->perde_jogo(j);
		mapa1->muda_posicao();
		printw("vidas:  %d   ",j);
		printw("movimentos:  %d",i);
		a= getch();
		jogador1->muda_posicao(true,a);
		j=mapa1->icolisao(j,jogador1->getPosx(), jogador1->getPosy());
		mob->perde_jogo(j);
		mapa1->scolisao(jogador1->getPosx(), jogador1->getPosy());
		mapa1->addInimigo(i,mob->aleatorio_x(),mob->aleatorio_y());
		i++;
		mapa1->rmvElemento( jogador1->getPosx(),jogador1->getPosy());
		refresh();

		endwin();
		}
		catch(char e)
		{
			endwin();
			break;
		}
		catch(int e){
			endwin();
			cout<<"parabens voce ganhou. digite seu nome para gravarmos quantos movimentos voce levou para ganhar."<<endl;
			string nome;
			ofstream file("doc//ranking.txt",fstream::app);
			getline(cin,nome);
			file << nome <<"  "<< i<< endl;
			file.close();


			break;
		}
		catch(double e){
			jogador1->muda_posicao(false,a);
			refresh();
			endwin();
		}
		catch(exception &e){
			endwin();
			cout << "voce perdeu"<< endl;
			break;
		}

	}
	delete(mob);
	delete(jogador1);
	delete(mapa1);
	return 0;
}

