#include "abstrata.hpp"

abstrata::abstrata() {
	setPosx(0);
	setPosy(0);
	setCharj('-');
}

void abstrata::setPosx(int posx) {
	this->posx = posx;
}

void abstrata::setPosy(int posy) {
	this->posy = posy;
}

void abstrata::setCharj(char charj) {
	this->charj = charj;
}

int abstrata::getPosx() {
	return posx;
}

int abstrata::getPosy() {
	return posy;
}

char abstrata::getCharj() {
	return charj;
}

void abstrata::muda_posicao(){}
