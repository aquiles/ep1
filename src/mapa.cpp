#include <iostream>
#include <string>
#include "mapa.hpp"
#include <fstream>
#include <ncurses.h>
#include <stdio_ext.h>

using namespace std;

mapa::mapa() {
	setPosy(3);
	setPosx(3);
}

mapa::~mapa() {
}

void mapa::setAlcance() {

	ifstream mapa("doc//maze_map.txt");

	string aux;

	for (int i = 0; i < 20; i++) {
		getline(mapa, aux);
		for (int u = 0; u < 50; u++) {
			this->alcance[i][u] = aux[u];
		}
	}

	mapa.close();
}



void mapa::addElemento(char charj, int posx, int posy) {

	this->alcance[posy][posx] = charj;

}

void mapa::muda_posicao() {
	for (int i = 0; i < 20; i++) {
			for (int u = 0; u < 50; u++) {
				printw("%c", this->alcance[i][u]);
				if (u >= 49) {
					printw("\n");
				}
			}
		}
	}


void mapa::rmvElemento(int posx, int posy) {
	for (int i = 0; i < 20; i++) {
		for (int u = 0; u < 50; u++) {
			if (alcance[i][u] == '@' && alcance[i][u] != alcance[posy][posx])
				alcance[i][u] = '-';
		}
	}
}

void mapa::abrePorta() {
	int n = 0;
	for (int i = 0; i < 20; i++) {
		for (int u = 0; u < 50; u++) {
			if (alcance[i][u] == 'K')
				n++;
		}
	}
	if (n == 0 && alcance[16][47] != '@')
		alcance[16][47] = '-';
}

void mapa::addInimigo(int i, int posx, int posy) {
	if (i % 2 == 0) {
		if(alcance[posy][posx]=='K')
			alcance[posy][posx+1] = '#';
		else if(alcance[posy][posx]==alcance[16][47])
			alcance[posy][posx-1]='#';
		else
			alcance[posy][posx]='#';
	}

}

void mapa::ganhaJogo() {
	if (alcance[16][48] == '@')
		throw(1);
}

void mapa::scolisao(int posx, int posy) {
	if (alcance[posy][posx] == '=')
		throw(1.1);
}

int mapa::icolisao(int j,int posx, int posy){
	if(alcance[posy][posx]=='#')
		return j=j-1;
	else
		return j;
}



